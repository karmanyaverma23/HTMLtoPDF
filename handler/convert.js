const fs=require("fs");
var htmlParser=require("node-html-parser");
const nodeHtmlToImage = require('node-html-to-image')
var pdf = require('html-pdf');
const utils = require('util')
const hb = require('handlebars')
const logger=require("../logger");


const convert=function(req,res){
logger.info("converting pdf");
  var name=req.params.name;
  const file=fs.readFileSync(`./upload/${name}.html`).toString();
  const root=htmlParser.parse(file);
  var div=root.querySelector(".esc-receipt").toString();
  var cssimg=fs.readFileSync('./middleimg.html','utf-8');
  var csspdf=fs.readFileSync('./middle.html','utf-8');
  var htmlpdf=csspdf+div;
  var htmlimg=cssimg+div;
  // console.log(html);

  var options = { width:"50mm"};

  try{
    pdf.create(htmlpdf, options).toFile(`./output/pdf/${name}.pdf`, function(err, res) {
        if (err) return console.log(err);
        console.log(res);
        logger.info("HTML converted to PDF and saved at ./output/pdf");
      })

  res.send("PDF Converted");
  }
  catch(err){
    console.log(err);
  }
  try{
    nodeHtmlToImage({
        output:`./output/image/${name}.png`,
        html: htmlimg
    }).then(() => console.log('The image was created successfully!'))
    logger.info("HTML converted to image and saved at ./output/image")
  }
  catch(err){
    console.log(err);
  }



}
  module.exports={convert};
