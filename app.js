const express=require("express");
const app=express();
const bodyParser=require("body-parser");
const fs=require("fs");
var htmlParser=require("node-html-parser");
var pdf = require('html-pdf');
const logger=require("./logger");
const route=require("./routes/route");
const upload=require("express-fileupload");
var log4js = require('log4js');
const nodeHtmlToImage = require('node-html-to-image')
// app.use(bodyParser.json());

app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));

app.use(upload());
app.get("/upload",function(req,res){
  res.sendFile(__dirname+"/index.html");
  logger.info("Upload Route");
})
app.post("/upload",function(req,res){
  if(req.files){
    logger.info("File is being uploaded");
    var file=req.files.file;
    var filename=file.name;
    console.log(filename);
    file.mv("./upload/"+filename,function(err){
      if(err){
        res.send(err)
      }
      else{
        res.send("File Uploaded");
        logger.info("File uploaded");
      }
    });
  }
})

app.use("/convert",route);


app.listen(3000,function(req,res){
  console.log("server is up at 3000");
})
